import Foundation

public class UserDefaultService {
  public static func setInvalidLogin(count: Int) {
    UserDefaults.standard.set(count, forKey: "invalidLogin")
  }
  
  public static func clearInvalidLogin() {
    UserDefaults.standard.removeObject(forKey: "invalidLogin")
  }
  
  public static func getLoginInvalidCount() -> Int {
    return Int(UserDefaults.standard.integer(forKey: "invalidLogin"))
  }
}
