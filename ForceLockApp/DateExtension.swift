import Foundation

extension Date {
  var timestamp: UInt64 {
    return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
  }
  
  init(timestamp: UInt64) {
    self.init(timeIntervalSince1970: Double(timestamp)/10_000_000 - 62_135_596_800)
  }
}
