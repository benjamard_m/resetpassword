import UIKit

class ResetPasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  
  @IBAction func onClickResetPassword(_ sender: Any) {
    UserDefaultService.clearInvalidLogin()
    redirectToLogin()
  }
  
  
  func redirectToLogin() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
    self.present(vc, animated: true, completion: nil)
  }
}
