import UIKit

class ViewController: UIViewController {
  
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var successLabel: UILabel!
  @IBOutlet weak var failLabel: UILabel!
  
  let limitTime = 3
  override func viewDidLoad() {
    super.viewDidLoad()
    successLabel.isHidden = true
    failLabel.isHidden = true
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    checkForLockApp()
  }
  
  @IBAction func onClickLogin(_ sender: Any) {
    let username = usernameTextField.text ?? ""
    let password = passwordTextField.text ?? ""
    
    if username == "username" && password == "password" {
      successLabel.isHidden = false
      failLabel.isHidden = true
      UserDefaultService.clearInvalidLogin()
    } else {
      manageLockAppInvalidLogin()
    }
  }
  
  
  func checkForLockApp() {
    if UserDefaultService.getLoginInvalidCount() >= limitTime {
      presentResetPasswordVC()
    }
  }
  
  func manageLockAppInvalidLogin() {
    guard UserDefaultService.getLoginInvalidCount() < limitTime else {
      alertLockingApp()
      return
    }
    let count = UserDefaultService.getLoginInvalidCount()
    UserDefaultService.setInvalidLogin(count: count + 1)
    failLabel.isHidden = false
    successLabel.isHidden = true
    failLabel.text = "FAILED \(UserDefaultService.getLoginInvalidCount())"
  }
  
  
  func alertLockingApp() {
    let alert = UIAlertController(title: "ขออภัย", message: "คุณใส่ข้อมูลผิดเกินจำนวนครั้งที่กำหนดกรุณากดลืมรหัสผ่าน", preferredStyle: .actionSheet)
    let alertAction = UIAlertAction(title: "ตกลง", style: .default) { (action) in
      self.presentResetPasswordVC()
    }
    alert.addAction(alertAction)
    self.present(alert, animated: true, completion: nil)
  }
  
  func presentResetPasswordVC() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
    self.present(vc, animated: true, completion: nil)
  }
}
